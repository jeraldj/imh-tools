// ==UserScript==
// @name        Private Reply
// @namespace   none
// @include     https://support.inmotionhosting.com/cgi-bin/staff.cgi?action=addresponse&ticket=*
// @require     https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js
// @version     1
// @grant       none
// ==/UserScript==

var newButton = '\
<li id="discardDraft" onclick=\'$("#PrivateResponse").val("Yes");$("#NotifyTicketCreator").val("No"); document.quickform.submit();\'>\
  <a href="javascript:void" class="LightBlueButton">\
    <span>PR</span>\
  </a>\
</li>';
$(".Buttons").html($(".Buttons").html() + newButton);
