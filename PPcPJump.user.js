// ==UserScript==
// @description Add cPJump buttons to PowerPanel
// @name PPcPJump
// @namespace PPcPJump
// @version 2.6
// @author Taken Over by Jerald J.
// @include https://secure1.inmotionhosting.com/admin/note/*
// @include https://cpjump.inmotionhosting.com/cplogin/
// @updateURL http://172.16.6.192/cpjump.user.js
// @downloadURL http://172.16.6.192/cpjump.user.js
// @match https://secure1.inmotionhosting.com/admin/note/*
// @match https://cpjump.inmotionhosting.com/cplogin/

// 2.2
// Add support for multiple tabs

// 2.3
// Fix cPJump Buttons lolherp

// 2.4
// Add Shared Support

// 2.5
// Add update URL

// 2.6 
// Test Updates

// ==/UserScript==

function setupButtons(){

        function clearSelection(){
            
        if(window.getSelection){
            if(window.getSelection.empty){
                window.getSelection.empty();
            } else if (window.getSelection().removeAllRanges){
                window.getSelection().removeAllRanges();
            }
        } else if (document.selection) {
            document.selection.empty();
        }
    };
    
    function copy_t2c_button_action(){
        
        var esc_string = `
        *T2C Escalation<br />
        Server: ${server}<br />
        User: ${username}<br />
        Domain Name: ${domain}<br />
        Affected URL:<br />
        <br />
        Agent: ${team_member}<br />
        Tier @C Escalation Point:<br />
        PP Link: ${URL}<br />
        Verified:<br />
        Emergency:<br />
        <br />
        Issue or Request:<br />
        Troubleshooting:<br />
        `;
        clearSelection();
        var esc_text = document.createElement('div');
        esc_text.innerHTML = esc_string;
        document.body.appendChild(esc_text);
        var range = document.createRange();
        range.selectNode(esc_text);
        window.getSelection().addRange(range);
        document.execCommand('copy');
        document.body.removeChild(esc_text);

    };

    function quick_copy_button_action(){
        var quick_copy_str = `${username} / ${server} / ${domain}`
        var quick_copy_text = document.createTextNode(quick_copy_str);
        clearSelection();
        document.body.appendChild(quick_copy_text);
        var range = document.createRange();
        range.selectNode(quick_copy_text);
        window.getSelection().addRange(range);
        document.execCommand('copy');
        document.body.removeChild(quick_copy_text);
    };
    
    function drawButtons(acct_tab_id){
        var account_tools = document.getElementsByClassName('acct_tools')[acct_tab_id];
        if (account_tools == undefined) return;
        if(account_tools.childNodes.length === 15){
            var re = new RegExp("ecbiz\|hub\|gs|res|esres");
            if(re.test(server) === false){
                var whm_button_container = document.createElement('li');
                var whm_form = document.createElement("form");
                whm_form.setAttribute("target", "_blank");
                whm_form.setAttribute("method", "POST");
                whm_form.setAttribute("action", "https://cpjump.inmotionhosting.com/cplogin/");
                var whm_serverField = document.createElement("input");
                whm_serverField.setAttribute("type", "hidden");
                whm_serverField.setAttribute("name", "server");
                whm_serverField.setAttribute("value", server);
                whm_form.appendChild(whm_serverField);
                var whm_userField = document.createElement("input");
                whm_userField.setAttribute("type", "hidden");
                whm_userField.setAttribute("name", "username");
                whm_userField.setAttribute("value", username);
                var imh_re = new RegExp("biz\|gs");

                whm_form.appendChild(whm_userField);
                var whm_serviceField = document.createElement("input");
                whm_serviceField.setAttribute("type", "hidden");
                whm_serviceField.setAttribute("name", "service");
                whm_serviceField.setAttribute("value", "whostmgrd");
                whm_form.appendChild(whm_serviceField);
                var whm_submit = document.createElement("button");
                whm_submit.setAttribute("type", "submit");
                whm_submit.setAttribute("name", "submit");
                whm_submit.innerHTML = "WHM";
                whm_form.appendChild(whm_submit);
                whm_button_container.appendChild(whm_form);
                account_tools.appendChild(whm_button_container);
                
                
                                var whm_button_container = document.createElement('li');
                var whm_form = document.createElement("form");
                whm_form.setAttribute("target", "_blank");
                whm_form.setAttribute("method", "POST");
                whm_form.setAttribute("action", "https://cpjump.inmotionhosting.com/cplogin/");
                var whm_serverField = document.createElement("input");
                whm_serverField.setAttribute("type", "hidden");
                whm_serverField.setAttribute("name", "server");
                whm_serverField.setAttribute("value", server);
                whm_form.appendChild(whm_serverField);
                var whm_userField = document.createElement("input");
                whm_userField.setAttribute("type", "hidden");
                whm_userField.setAttribute("name", "username");
                whm_userField.setAttribute("value", 'root');
                var imh_re = new RegExp("biz\|gs");

                whm_form.appendChild(whm_userField);
                var whm_serviceField = document.createElement("input");
                whm_serviceField.setAttribute("type", "hidden");
                whm_serviceField.setAttribute("name", "service");
                whm_serviceField.setAttribute("value", "whostmgrd");
                whm_form.appendChild(whm_serviceField);
                var whm_submit = document.createElement("button");
                whm_submit.setAttribute("type", "submit");
                whm_submit.setAttribute("name", "submit");
                whm_submit.innerHTML = "WHM ROOT";
                whm_form.appendChild(whm_submit);
                whm_button_container.appendChild(whm_form);
                account_tools.appendChild(whm_button_container);
                
                                var cpanel_button_container = document.createElement('li');
                var cpanel_form = document.createElement("form");
                cpanel_form.setAttribute("target", "_blank");
                cpanel_form.setAttribute("method", "POST");
                cpanel_form.setAttribute("action", "https://cpjump.inmotionhosting.com/dedtmpkeys/process-dedkey.php");
                var cpanel_serverField = document.createElement("input");
                cpanel_serverField.setAttribute("type", "hidden");
                cpanel_serverField.setAttribute("name", "server");
                cpanel_serverField.setAttribute("value", server);
                cpanel_form.appendChild(cpanel_serverField);
                var cpanel_userField = document.createElement("input");
                cpanel_userField.setAttribute("type", "hidden");
                cpanel_userField.setAttribute("name", "port");
                cpanel_userField.setAttribute("value", "22");
                cpanel_form.appendChild(cpanel_userField);
                var cpanel_submit = document.createElement("button");
                cpanel_submit.setAttribute("type", "submit");
                cpanel_submit.setAttribute("name", "submit");
                cpanel_submit.innerHTML = "dKey";
                cpanel_form.appendChild(cpanel_submit);
                cpanel_button_container.appendChild(cpanel_form);
                account_tools.appendChild(cpanel_button_container);
                
                var cpanel_button_container = document.createElement('li');
                var cpanel_form = document.createElement("form");
                cpanel_form.setAttribute("target", "_blank");
                cpanel_form.setAttribute("method", "POST");
                cpanel_form.setAttribute("action", "https://cpjump.inmotionhosting.com/cplogin/");
                var cpanel_serverField = document.createElement("input");
                cpanel_serverField.setAttribute("type", "hidden");
                cpanel_serverField.setAttribute("name", "server");
                cpanel_serverField.setAttribute("value", server);
                cpanel_form.appendChild(cpanel_serverField);
                var cpanel_userField = document.createElement("input");
                cpanel_userField.setAttribute("type", "hidden");
                cpanel_userField.setAttribute("name", "username");
                cpanel_userField.setAttribute("value", username);
                cpanel_form.appendChild(cpanel_userField);
                var cpanel_serviceField = document.createElement("input");
                cpanel_serviceField.setAttribute("type", "hidden");
                cpanel_serviceField.setAttribute("name", "service");
                cpanel_serviceField.setAttribute("value", "cpaneld");
                cpanel_form.appendChild(cpanel_serviceField);
                var cpanel_submit = document.createElement("button");
                cpanel_submit.setAttribute("type", "submit");
                cpanel_submit.setAttribute("name", "submit");
                cpanel_submit.innerHTML = "cPanel";
                cpanel_form.appendChild(cpanel_submit);
                cpanel_button_container.appendChild(cpanel_form);
                account_tools.appendChild(cpanel_button_container);
            } else {
                
                var whm_button_container = document.createElement('li');
                var whm_form = document.createElement("form");
                whm_form.setAttribute("target", "_blank");
                whm_form.setAttribute("method", "POST");
                whm_form.setAttribute("action", "https://cpjump.inmotionhosting.com/cplogin/");
                var whm_serverField = document.createElement("input");
                whm_serverField.setAttribute("type", "hidden");
                whm_serverField.setAttribute("name", "server");
                whm_serverField.setAttribute("value", server);
                whm_form.appendChild(whm_serverField);
                var whm_userField = document.createElement("input");
                whm_userField.setAttribute("type", "hidden");
                whm_userField.setAttribute("name", "username");
                whm_userField.setAttribute("value", "inmotion");
                var imh_re = new RegExp("biz\|gs");

                whm_form.appendChild(whm_userField);
                var whm_serviceField = document.createElement("input");
                whm_serviceField.setAttribute("type", "hidden");
                whm_serviceField.setAttribute("name", "service");
                whm_serviceField.setAttribute("value", "whostmgrd");
                whm_form.appendChild(whm_serviceField);
                var whm_submit = document.createElement("button");
                whm_submit.setAttribute("type", "submit");
                whm_submit.setAttribute("name", "submit");
                whm_submit.innerHTML = "WHM";
                whm_form.appendChild(whm_submit);
                whm_button_container.appendChild(whm_form);
                account_tools.appendChild(whm_button_container);
                
                
                                var whm_button_container = document.createElement('li');
                var whm_form = document.createElement("form");
                whm_form.setAttribute("target", "_blank");
                whm_form.setAttribute("method", "POST");
                whm_form.setAttribute("action", "https://cpjump.inmotionhosting.com/cplogin/");
                var whm_serverField = document.createElement("input");
                whm_serverField.setAttribute("type", "hidden");
                whm_serverField.setAttribute("name", "server");
                whm_serverField.setAttribute("value", server);
                whm_form.appendChild(whm_serverField);
                var whm_userField = document.createElement("input");
                whm_userField.setAttribute("type", "hidden");
                whm_userField.setAttribute("name", "username");
                whm_userField.setAttribute("value", username);
                var imh_re = new RegExp("biz\|gs");

                whm_form.appendChild(whm_userField);
                var whm_serviceField = document.createElement("input");
                whm_serviceField.setAttribute("type", "hidden");
                whm_serviceField.setAttribute("name", "service");
                whm_serviceField.setAttribute("value", "whostmgrd");
                whm_form.appendChild(whm_serviceField);
                var whm_submit = document.createElement("button");
                whm_submit.setAttribute("type", "submit");
                whm_submit.setAttribute("name", "submit");
                whm_submit.innerHTML = "WHM Reseller";
                whm_form.appendChild(whm_submit);
                whm_button_container.appendChild(whm_form);
                account_tools.appendChild(whm_button_container);
                
                
                var cpanel_button_container = document.createElement('li');
                var cpanel_form = document.createElement("form");
                cpanel_form.setAttribute("target", "_blank");
                cpanel_form.setAttribute("method", "POST");
                cpanel_form.setAttribute("action", "https://cpjump.inmotionhosting.com/cplogin/");
                var cpanel_serverField = document.createElement("input");
                cpanel_serverField.setAttribute("type", "hidden");
                cpanel_serverField.setAttribute("name", "server");
                cpanel_serverField.setAttribute("value", server);
                cpanel_form.appendChild(cpanel_serverField);
                var cpanel_userField = document.createElement("input");
                cpanel_userField.setAttribute("type", "hidden");
                cpanel_userField.setAttribute("name", "username");
                cpanel_userField.setAttribute("value", username);
                cpanel_form.appendChild(cpanel_userField);
                var cpanel_serviceField = document.createElement("input");
                cpanel_serviceField.setAttribute("type", "hidden");
                cpanel_serviceField.setAttribute("name", "service");
                cpanel_serviceField.setAttribute("value", "cpaneld");
                cpanel_form.appendChild(cpanel_serviceField);
                var cpanel_submit = document.createElement("button");
                cpanel_submit.setAttribute("type", "submit");
                cpanel_submit.setAttribute("name", "submit");
                cpanel_submit.innerHTML = "cPanel";
                cpanel_form.appendChild(cpanel_submit);
                cpanel_button_container.appendChild(cpanel_form);
                account_tools.appendChild(cpanel_button_container);
                
            }
            var copy_t2c_button_container = document.createElement('li');
            var copy_t2c_button = document.createElement('button');
            copy_t2c_button.type = 'button';
            copy_t2c_button.name = 'copy_t2c_button';
            copy_t2c_button.innerHTML = 'Copy T2C';
            copy_t2c_button.onclick = copy_t2c_button_action;
            copy_t2c_button.keydown = copy_t2c_button_action;
            copy_t2c_button_container.appendChild(copy_t2c_button);

            var quick_copy_button_container = document.createElement('li');
            var quick_copy_button = document.createElement('button');
            quick_copy_button.type = 'button';
            quick_copy_button.name = 'quick_copy_button'
            quick_copy_button.innerHTML = 'Quick Copy';
            quick_copy_button.onclick = quick_copy_button_action;
            quick_copy_button.keydown = quick_copy_button_action;
            quick_copy_button_container.appendChild(quick_copy_button);

            account_tools.appendChild(copy_t2c_button_container);
            account_tools.appendChild(quick_copy_button_container);
        }
    };
    
    var data = document.getElementsByClassName('acct_info');
    for (i = 0; i < data.length; i++) { 
      var username_data = data[i].getElementsByClassName('acct_username');
      var server_data = data[i].getElementsByClassName('acct_server');
      var team_member = document.getElementById('hiddenAdmin').innerText;
      var URL = document.URL;
      var username = username_data[0].innerText;
      var server = server_data[0].innerText;
      var domain = document.getElementsByClassName('acct_domain')[i].innerText;
      var current_acct_tab = document.getElementsByClassName('account_tabs')[i].id;
      var id_offset = 24;
      drawButtons(i);
    }

    function updateData(acct_tab_id){
        data = document.getElementsByClassName('acct_info');
        username_data = data[acct_tab_id].getElementsByClassName('acct_username');
        server_data = data[acct_tab_id].getElementsByClassName('acct_server');
        username = username_data[0].innerText;
        server = server_data[0].innerText;
        domain = document.getElementsByClassName('acct_domain')[acct_tab_id].innerText;
    };

    document.addEventListener('click', function(e){
        e = e || window.event;
        var target = e.target || e.srcElement;
        if(target.id && target.id.slice(0, -2) === 'ui-id-') { //} && target.id !== current_acct_tab){
                var target_id_num_str = target.id.slice(-2);
                var target_id_num = parseInt(target_id_num_str);
                var id_diff = target_id_num - 24;
                drawButtons(id_diff-1);
                updateData(id_diff-1);
                current_acct_tab = target.id;
            }
        }, false);
};

setupButtons();


