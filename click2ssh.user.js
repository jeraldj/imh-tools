// ==UserScript==
// @name        Click To SSH
// @namespace   none
// @include     https://secure1.inmotionhosting.com/admin/note/id/*
// @version     1
// @grant       none
// ==/UserScript==
$(".acct_server").each(function() {
  server=$(this);
  user=$(this).parent().parent().children("li").children(".acct_username").html();
  host = server.html();
  domain = $(this).parent().parent().parent().parent().parent().children(".customer_details").children(".cust_details").children("li").children("ul").children("li").children(".acct_domain").html();
link = "<a href='imh://"+user+":"+domain+"@"+host+"'>"+host+"</a>";
server.html(link);
});

$(".copyBtn").hide();
$(".copyRevTemp").show();


